package spgangguanemosional;

import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JOptionPane;

public class MenuUtama extends javax.swing.JFrame {
    public static String nama;
    public MenuUtama() {
        initComponents();
        setName("Sistem Pakar Diagnosis Gangguan Emosional Anak");
        setSize(629, 390);
    }
    
    public void setNama(String nama){
        this.nama = nama;
    }
    
    public static String getNama(){
        return nama;
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField1 = new javax.swing.JTextField();
        next = new javax.swing.JLabel();
        background = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(629, 390));
        setResizable(false);
        setSize(new java.awt.Dimension(629, 390));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
        });
        getContentPane().setLayout(null);

        jTextField1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jTextField1.setMargin(new java.awt.Insets(2, 10, 2, 2));
        getContentPane().add(jTextField1);
        jTextField1.setBounds(120, 230, 320, 40);

        next.setIcon(new javax.swing.ImageIcon(getClass().getResource("/spgangguanemosional/Gambar/next.JPG"))); // NOI18N
        next.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        next.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                nextMouseClicked(evt);
            }
        });
        getContentPane().add(next);
        next.setBounds(450, 230, 80, 37);

        background.setIcon(new javax.swing.ImageIcon(getClass().getResource("/spgangguanemosional/Gambar/tampilan awal AI copy.JPG"))); // NOI18N
        getContentPane().add(background);
        background.setBounds(0, 0, 630, 390);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void nextMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_nextMouseClicked
        if(jTextField1.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Silahkan Masukkan Nama Terlebih dahulu !", "Tolong Masukkan Nama!", JOptionPane.WARNING_MESSAGE);
        }
        else {
            setNama(jTextField1.getText());
            Interface tanya=new Interface();
            tanya.setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_nextMouseClicked

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
        Dimension posisi = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (posisi.width - this.getWidth())/2;
        int y = (posisi.height -this.getHeight())/2;
        this.setLocation(x,y);
    }//GEN-LAST:event_formWindowActivated

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MenuUtama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MenuUtama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MenuUtama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MenuUtama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MenuUtama().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel background;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JLabel next;
    // End of variables declaration//GEN-END:variables
}

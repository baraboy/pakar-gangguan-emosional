package spgangguanemosional;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class koneksi {

    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/gangguanemosional";
    static final String USER = "root";
    static final String PASS = "";
    Connection con;
    static ResultSet rs;
    
    public koneksi(){
        KoneksiDB();
    }
    
    public Connection KoneksiDB(){
        try {
            Class.forName(JDBC_DRIVER);
            con = DriverManager.getConnection(DB_URL, USER, PASS);
            return con;
        }catch (ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, "ERROR : " + e.getMessage());
            return null;
        }catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "ERROR : " + e.getMessage());
            return null;
        }catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR : " + e.getMessage());
            return null;
        }
    }
    
    public void tanya(String kode){
        try {
            Statement stmt=(Statement) con.createStatement();
            rs = stmt.executeQuery("SELECT NAMA_GEJALA FROM GEJALA WHERE GEJALAID='"+kode+"';");
            rs.next();
            String y = rs.getString("NAMA_GEJALA");
            Interface.jTextArea1.setText(y);
            stmt.close();
            rs.close();
        } catch (Exception e) {
           e.printStackTrace();
       }
    }

    public String kode(String kode){
        String x=null;
        try {
            Statement stmt=(Statement) con.createStatement();
            rs = stmt.executeQuery("SELECT GEJALAID FROM GEJALA WHERE NAMA_GEJALA='"+kode+"';");
            rs.next();
            x = rs.getString("GEJALAID");
            stmt.close();
            rs.close();
        } catch (Exception e) {
           e.printStackTrace();
       }
        return x;
    }
    
    public String gangguan(String kode){
        String x=null;
        try {
            Statement stmt=(Statement) con.createStatement();
            rs = stmt.executeQuery("SELECT NAMA_GANGGUAN FROM GANGGUAN WHERE GANGGUANID='"+kode+"';");
            rs.next();
            x = rs.getString("NAMA_GANGGUAN");
            stmt.close();
            rs.close();
        } catch (Exception e) {
           e.printStackTrace();
       }        
       return x;
    }

    public String gangguandeskripsi(String kode){
        String x=null;
        try {
            Statement stmt=(Statement) con.createStatement();
            rs = stmt.executeQuery("SELECT DEFINISI FROM GANGGUAN WHERE GANGGUANID='"+kode+"';");
            rs.next();
            x = rs.getString("DEFINISI");
            stmt.close();
            rs.close();
        } catch (Exception e) {
           e.printStackTrace();
       }        
       return x;
    }

    public String solusi(String kode){
        String x=null;
        try {
            Statement stmt=(Statement) con.createStatement();
            rs = stmt.executeQuery("SELECT SOLUSI FROM GANGGUAN WHERE GANGGUANID='"+kode+"';");
            rs.next();
            x = rs.getString("SOLUSI");
            stmt.close();
            rs.close();
        } catch (Exception e) {
           e.printStackTrace();
       }        
       return x;
    }    
    
}

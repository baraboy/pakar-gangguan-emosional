package spgangguanemosional;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JOptionPane;
public class Kesimpulan extends javax.swing.JFrame {
    String gangguan=Interface.getgangguan();
    koneksi konek=new koneksi();
    String nama=MenuUtama.getNama();
    String kode=Interface.getkode();
    public String tanya[]=new String[40];
    public String jawaban[]=new String[40];    
    String data;
    MenuUtama MU;
    
    public Kesimpulan() {
        initComponents();
        setName("Sistem Pakar Diagnosis Gangguan Emosional Anak");
        setSize(780, 460);
        tampilnama.setText(nama);
        jTextArea1.setText(gangguan);
        jTextArea2.setText(konek.gangguandeskripsi(kode));
    }
    
    public String result(){
        String tampung="";
        int c=Interface.resjawaban.size();
        int n=0;
        while(n<c){
            tampung = tampung + (n+1)+". "+Interface.restanya.get(n)+" = "+Interface.resjawaban.get(n)+"\n";
            n++;
        }
        return tampung;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea2 = new javax.swing.JTextArea();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextArea3 = new javax.swing.JTextArea();
        jButton1 = new javax.swing.JButton();
        solusi = new javax.swing.JLabel();
        tampilnama = new javax.swing.JLabel();
        Diagnosis = new javax.swing.JLabel();
        deskripsi = new javax.swing.JLabel();
        home = new javax.swing.JLabel();
        result = new javax.swing.JLabel();
        background = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setSize(new java.awt.Dimension(629, 390));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
        });
        getContentPane().setLayout(null);

        jScrollPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 0));

        jTextArea1.setEditable(false);
        jTextArea1.setColumns(20);
        jTextArea1.setRows(1);
        jTextArea1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jTextArea1.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        jScrollPane1.setViewportView(jTextArea1);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(260, 50, 260, 40);

        jScrollPane2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 0));
        jScrollPane2.setVerifyInputWhenFocusTarget(false);

        jTextArea2.setEditable(false);
        jTextArea2.setColumns(1);
        jTextArea2.setFont(new java.awt.Font("Nirmala UI", 0, 13)); // NOI18N
        jTextArea2.setForeground(new java.awt.Color(0, 153, 153));
        jTextArea2.setLineWrap(true);
        jTextArea2.setRows(3);
        jTextArea2.setWrapStyleWord(true);
        jTextArea2.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jTextArea2.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        jTextArea2.setEnabled(false);
        jScrollPane2.setViewportView(jTextArea2);

        getContentPane().add(jScrollPane2);
        jScrollPane2.setBounds(160, 130, 440, 90);

        jScrollPane3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 0));
        jScrollPane3.setVerifyInputWhenFocusTarget(false);

        jTextArea3.setEditable(false);
        jTextArea3.setColumns(1);
        jTextArea3.setFont(new java.awt.Font("Nirmala UI", 0, 13)); // NOI18N
        jTextArea3.setForeground(new java.awt.Color(0, 153, 153));
        jTextArea3.setLineWrap(true);
        jTextArea3.setRows(3);
        jTextArea3.setWrapStyleWord(true);
        jTextArea3.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jTextArea3.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        jTextArea3.setEnabled(false);
        jScrollPane3.setViewportView(jTextArea3);

        getContentPane().add(jScrollPane3);
        jScrollPane3.setBounds(160, 250, 440, 90);

        jButton1.setBackground(new java.awt.Color(255, 255, 255));
        jButton1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButton1.setForeground(new java.awt.Color(0, 153, 153));
        jButton1.setText("Solusi");
        jButton1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1);
        jButton1.setBounds(330, 350, 100, 30);

        solusi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/spgangguanemosional/Gambar/Solusi.jpg"))); // NOI18N
        getContentPane().add(solusi);
        solusi.setBounds(160, 220, 440, 120);

        tampilnama.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tampilnama.setText("jLabel2");
        getContentPane().add(tampilnama);
        tampilnama.setBounds(260, 20, 260, 14);

        Diagnosis.setIcon(new javax.swing.ImageIcon(getClass().getResource("/spgangguanemosional/Gambar/image.jpg"))); // NOI18N
        getContentPane().add(Diagnosis);
        Diagnosis.setBounds(260, 50, 260, 40);

        deskripsi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/spgangguanemosional/Gambar/rekomendasi.jpg"))); // NOI18N
        getContentPane().add(deskripsi);
        deskripsi.setBounds(160, 100, 440, 120);

        home.setIcon(new javax.swing.ImageIcon(getClass().getResource("/spgangguanemosional/Gambar/home.jpg"))); // NOI18N
        home.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        home.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                homeMouseClicked(evt);
            }
        });
        getContentPane().add(home);
        home.setBounds(230, 350, 90, 30);

        result.setIcon(new javax.swing.ImageIcon(getClass().getResource("/spgangguanemosional/Gambar/result.jpg"))); // NOI18N
        result.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        result.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                resultMouseClicked(evt);
            }
        });
        getContentPane().add(result);
        result.setBounds(440, 350, 90, 30);

        background.setIcon(new javax.swing.ImageIcon(getClass().getResource("/spgangguanemosional/Gambar/bg2.jpg"))); // NOI18N
        background.setText("asjdbk");
        getContentPane().add(background);
        background.setBounds(0, -10, 780, 450);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void resultMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_resultMouseClicked
        data=result();
        int i=0;
        JOptionPane.showMessageDialog(null, "History yang terjadi yaitu \n"+data, 
                                    "History Interface dan Jawaban" ,JOptionPane.PLAIN_MESSAGE);
    }//GEN-LAST:event_resultMouseClicked

    private void homeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_homeMouseClicked
        MU=new MenuUtama();
        MU.setVisible(true);
        dispose();
    }//GEN-LAST:event_homeMouseClicked

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
        Dimension posisi = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (posisi.width - this.getWidth())/2;
        int y = (posisi.height -this.getHeight())/2;
        this.setLocation(x,y);
    }//GEN-LAST:event_formWindowActivated

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        jTextArea3.setText(konek.solusi(kode));
    }//GEN-LAST:event_jButton1ActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Kesimpulan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Kesimpulan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Kesimpulan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Kesimpulan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Kesimpulan().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Diagnosis;
    private javax.swing.JLabel background;
    private javax.swing.JLabel deskripsi;
    private javax.swing.JLabel home;
    private javax.swing.JButton jButton1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextArea jTextArea2;
    private javax.swing.JTextArea jTextArea3;
    private javax.swing.JLabel result;
    private javax.swing.JLabel solusi;
    private javax.swing.JLabel tampilnama;
    // End of variables declaration//GEN-END:variables
}

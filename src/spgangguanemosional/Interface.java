package spgangguanemosional;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.ArrayList;
import javafx.scene.control.TextArea;
import javax.swing.ImageIcon;

public class Interface extends javax.swing.JFrame {
    public String gejala[]=new String[40];
    public String diagnosis[]=new String[13];
    public ArrayList<String> jawaban = new ArrayList<>();
    public static ArrayList<String> restanya = new ArrayList<>();
    public static ArrayList<String> resjawaban = new ArrayList<>();
    boolean selesai=false, j1=false, j2=false;
    String ya,tidak, soal, jawab, sim1="g09", sim2;
    public static String gangguan, kode;
    private koneksi konek;
    public Kesimpulan simpul;
    int jumsoal=0;

    public Interface() {
        initComponents();
        setName("Sistem Pakar Diagnosis Gangguan Emosional Anak");
        setSize(780, 460);
        konek = new koneksi();
        inisialgejala();
        inisialdiagnosis();
        kosongkan();
        pertanyaan(gejala[8]);
        restanya.add("Apakah anak anda dalam pengaruh obat-obatan ?");
    }

    public void inisialgejala(){
        gejala[0]="g01";        gejala[1]="g02";        gejala[2]="g03";        gejala[3]="g04";
        gejala[4]="g05";        gejala[5]="g06";        gejala[6]="g07";        gejala[7]="g08";
        gejala[8]="g09";        gejala[9]="g10";        gejala[10]="g11";       gejala[11]="g12";
        gejala[12]="g13";       gejala[13]="g14";       gejala[14]="g15";       gejala[15]="g16";
        gejala[16]="g17";       gejala[17]="g18";       gejala[18]="g19";       gejala[19]="g20";
        gejala[20]="g21";       gejala[21]="g22";       gejala[22]="g23";       gejala[23]="g24";
        gejala[24]="g25";       gejala[25]="g26";       gejala[26]="g27";       gejala[27]="g28";
        gejala[28]="g29";       gejala[29]="g30";       gejala[30]="g31";       gejala[31]="g32";
        gejala[32]="g33";       gejala[33]="g34";       gejala[34]="g35";       gejala[35]="g36";
        gejala[36]="g37";       gejala[37]="g38";       gejala[38]="g39";       gejala[39]="g40";
    }    

    public void inisialdiagnosis(){
        diagnosis[0]="A01";
        diagnosis[1]="A02";
        diagnosis[2]="A03";
        diagnosis[3]="A04";
        diagnosis[4]="A05";
        diagnosis[5]="A06";
        diagnosis[6]="A07";
        diagnosis[7]="A08";
        diagnosis[8]="A09";
        diagnosis[9]="A10";
        diagnosis[10]="A11";
        diagnosis[11]="A12";
        diagnosis[12]="A13";
    }    

    public void cekkesimpulan(){
        int y= jawaban.size();
        rule1(y);
        rule2(y);
        rule3(y);
        rule4(y);
        rule5(y);
        rule6(y);
        rule7(y);
        rule8(y);
        rule9(y);
        rule10(y);        
        rulehabis();
    }

    public void rule1(int m){
        if(m>5){
            String a=jawaban.get(0);
            String b=jawaban.get(1);
            String c=jawaban.get(2);
            String d=jawaban.get(3);
            String e=jawaban.get(4);
            String f=jawaban.get(5);
            if(     a.equals("g02")&&
                    b.equals("g01")&&
                    c.equals("g05")&&
                    d.equals("g06")&&
                    e.equals("g04")&&
                    f.equals("g03")
                    ){
                gangguan = konek.gangguan(diagnosis[0]);
                kode=diagnosis[0];
                simpul=new Kesimpulan();
                simpul.setVisible(true);
                dispose();
            }
        }
    }

    public void rule2(int m){
        if(m>4){
            String a=jawaban.get(0);
            String b=jawaban.get(1);
            String c=jawaban.get(2);
            String d=jawaban.get(3);
            String e=jawaban.get(4);
            if(     a.equals("g02")&&
                    b.equals("g01")&&
                    c.equals("g05")&&
                    d.equals("g06")&&
                    e.equals("g07")
                    ){
                gangguan = konek.gangguan(diagnosis[1]);
                kode=diagnosis[1];
                simpul=new Kesimpulan();
                simpul.setVisible(true);
                dispose();
            }
        }
    }

    public void rule3(int m){
        if(m>4){
            String a=jawaban.get(0);
            String b=jawaban.get(1);
            String c=jawaban.get(2);
            String d=jawaban.get(3);
            String e=jawaban.get(4);
            if(     a.equals("g02")&&
                    b.equals("g01")&&
                    c.equals("g05")&&
                    d.equals("g06")&&
                    e.equals("g08")
                    ){
                gangguan = konek.gangguan(diagnosis[2]);
                kode=diagnosis[2];
                simpul=new Kesimpulan();
                simpul.setVisible(true);
                dispose();
            }
        }
    }

    public void rule4(int m){
        if(m>1){
            String a=jawaban.get(0);
            String b=jawaban.get(1);
            if(     a.equals("g02")&&
                    b.equals("g10")
                    ){
                gangguan = konek.gangguan(diagnosis[3]);
                kode=diagnosis[3];
                simpul=new Kesimpulan();
                simpul.setVisible(true);
                dispose();
            }
        }
    }

    public void rule5(int m){
        if(m>7){
            String a=jawaban.get(0);
            String b=jawaban.get(1);
            String c=jawaban.get(2);
            String d=jawaban.get(3);
            String e=jawaban.get(4);
            String f=jawaban.get(5);
            String g=jawaban.get(6);
            String h=jawaban.get(7);
            if(     a.equals("g11")&&
                    b.equals("g12")&&
                    c.equals("g13")&&
                    d.equals("g14")&&
                    e.equals("g15")&&
                    f.equals("g16")&&
                    g.equals("g33")&&
                    h.equals("g38")
                    ){
                gangguan = konek.gangguan(diagnosis[4]);
                kode=diagnosis[4];
                simpul=new Kesimpulan();
                simpul.setVisible(true);
                dispose();
            }
        }
    }    

    public void rule6(int m){
        if(m>3){
            String a=jawaban.get(0);
            String b=jawaban.get(1);
            String c=jawaban.get(2);
            String d=jawaban.get(3);
            if(     a.equals("g02")&&
                    b.equals("g01")&&
                    c.equals("g37")&&
                    d.equals("g39")
                    ){
                gangguan = konek.gangguan(diagnosis[5]);
                kode=diagnosis[5];
                simpul=new Kesimpulan();
                simpul.setVisible(true);
                dispose();
            }
        }
    }    
    
    public void rule7(int m){
        if(m>9){
            String a=jawaban.get(0);
            String b=jawaban.get(1);
            String c=jawaban.get(2);
            String d=jawaban.get(3);
            String e=jawaban.get(4);
            String f=jawaban.get(5);
            String g=jawaban.get(6);
            String h=jawaban.get(7);
            String i=jawaban.get(8);
            String j=jawaban.get(9);
            if(     a.equals("g40")&&
                    b.equals("g23")&&
                    c.equals("g24")&&
                    d.equals("g25")&&
                    e.equals("g26")&&
                    f.equals("g27")&&
                    g.equals("g28")&&
                    h.equals("g29")&&
                    i.equals("g30")&&
                    j.equals("g18")
                    ){
                gangguan = konek.gangguan(diagnosis[6]);
                kode=diagnosis[6];
                simpul=new Kesimpulan();
                simpul.setVisible(true);
                dispose();
            }
        }
    }    

    public void rule8(int m){
        if(m>4){
            String a=jawaban.get(0);
            String b=jawaban.get(1);
            String c=jawaban.get(2);
            String d=jawaban.get(3);
            String e=jawaban.get(4);
            if(     a.equals("g11")&&
                    b.equals("g17")&&
                    c.equals("g19")&&
                    d.equals("g22")&&
                    e.equals("g35")
                    ){
                gangguan = konek.gangguan(diagnosis[7]);
                kode=diagnosis[7];
                simpul=new Kesimpulan();
                simpul.setVisible(true);
                dispose();
            }
        }
    }        

    public void rule9(int m){
        if(m>3){
            String a=jawaban.get(0);
            String b=jawaban.get(1);
            String c=jawaban.get(2);
            String d=jawaban.get(3);
            if(     a.equals("g20")&&
                    b.equals("g21")&&
                    c.equals("g34")&&
                    d.equals("g36")
                    ){
                gangguan = konek.gangguan(diagnosis[8]);
                kode=diagnosis[8];
                simpul=new Kesimpulan();
                simpul.setVisible(true);
                dispose();
            }
        }
    }    

    public void rule10(int m){
        if(m>10){
            String a=jawaban.get(0);
            String b=jawaban.get(1);
            String c=jawaban.get(2);
            String d=jawaban.get(3);
            String e=jawaban.get(4);
            String f=jawaban.get(5);
            String g=jawaban.get(6);
            String h=jawaban.get(7);
            String i=jawaban.get(8);
            String j=jawaban.get(9);
            String k=jawaban.get(10);
            if(     a.equals("g40")&&
                    b.equals("g23")&&
                    c.equals("g24")&&
                    d.equals("g25")&&
                    e.equals("g26")&&
                    f.equals("g27")&&
                    g.equals("g28")&&
                    h.equals("g29")&&
                    i.equals("g30")&&
                    j.equals("g31")&&
                    k.equals("g32")
                    ){
                gangguan = konek.gangguan(diagnosis[9]);
                kode=diagnosis[9];
                simpul=new Kesimpulan();
                simpul.setVisible(true);
                dispose();
            }
        }
    }    
    
    public void rulehabis(){
        if(selesai==true){
            if(jawab.equals("ya")){
                gangguan = konek.gangguan(diagnosis[10]);
                kode=diagnosis[10];
            }
            else if(jawab.equals("tidak")){
                int x=resjawaban.size();
                int y=0, jum=0;
                boolean normal=false;
                while(y<x){
                    String z=resjawaban.get(y);
                    if(z.equals("Ya")){
                        normal=true;
                    }
                    y++;
                }
                if(normal==true){
                    gangguan = konek.gangguan(diagnosis[12]);
                    kode=diagnosis[12];
                }
                else if(normal==false){
                    gangguan = konek.gangguan(diagnosis[11]);
                    kode=diagnosis[11];
                }
            }
            simpul=new Kesimpulan();
            simpul.setVisible(true);
            dispose();
        }
    }
    
    public static String getgangguan(){
        return gangguan;
    }

    public static String getkode(){
        return kode;
    }
    
    public void pertanyaan(String x){
        image.setIcon(new javax.swing.ImageIcon(getClass().getResource(x+".jpg")));
        String asd=Integer.toString(jumsoal+1);
        jLabel4.setText(asd);
        konek.tanya(x);
        soal=jTextArea1.getText();
        sim1=x;
        if(sim1==sim2){
            selesai=true;
        }
        if(j1==true){
            restanya.add(jTextArea1.getText());
            resjawaban.add(pilihya.getText());            
            jawab="ya";
        }
        else if (j2==true){
            restanya.add(jTextArea1.getText());
            resjawaban.add(pilihtidak.getText());            
            jawab="tidak";
        }
        if(x.equals(gejala[0])){
            ya=gejala[4];
            tidak=gejala[9];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                selesai=false;
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[1])){
            ya=gejala[0];
            tidak=gejala[10];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                selesai=false;
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[2])){
            ya=gejala[2];
            tidak=gejala[2];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[3])){
            ya=gejala[2];
            tidak=gejala[6];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                selesai=false;
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[4])){
            ya=gejala[5];
            tidak=gejala[36];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                selesai=false;
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[5])){
            ya=gejala[3];
            tidak=gejala[5];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[6])){
            ya=gejala[6];
            tidak=gejala[7];
            if( j1==true){
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                selesai=false;
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[7])){
            ya=gejala[7];
            tidak=gejala[7];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                resjawaban.add(pilihtidak.getText());
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[8])){
            ya=gejala[8];
            tidak=gejala[1];
            if( j1==true){
                j1=false;
                selesai=true;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                selesai=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[9])){
            ya=gejala[9];
            tidak=gejala[9];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[10])){
            ya=gejala[11];
            tidak=gejala[39];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                selesai=false;
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[11])){
            ya=gejala[12];
            tidak=gejala[16];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                selesai=false;
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[12])){
            ya=gejala[13];
            tidak=gejala[12];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[13])){
            ya=gejala[14];
            tidak=gejala[13];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[14])){
            ya=gejala[15];
            tidak=gejala[14];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[15])){
            ya=gejala[32];
            tidak=gejala[15];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[16])){
            ya=gejala[18];
            tidak=gejala[16];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
           }
        }
        else if(x.equals(gejala[17])){
            ya=gejala[17];
            tidak=gejala[17];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[18])){
            ya=gejala[21];
            tidak=gejala[18];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[19])){
            ya=gejala[20];
            tidak=gejala[19];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
                selesai=true;
            }
        }
        else if(x.equals(gejala[20])){
            ya=gejala[33];
            tidak=gejala[20];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[21])){
            ya=gejala[34];
            tidak=gejala[21];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[22])){
            ya=gejala[23];
            tidak=gejala[22];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[23])){
            ya=gejala[24];
            tidak=gejala[23];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[24])){
            ya=gejala[25];
            tidak=gejala[24];
            if( j1==true){
                j1=false;
                selesai=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[25])){
            ya=gejala[26];
            tidak=gejala[25];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[26])){
            ya=gejala[27];
            tidak=gejala[26];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[27])){
            ya=gejala[28];
            tidak=gejala[27];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[28])){
            ya=gejala[29];
            tidak=gejala[28];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[29])){
            ya=gejala[30];
            tidak=gejala[29];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[30])){
            ya=gejala[31];
            tidak=gejala[17];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                selesai=false;
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[31])){
            ya=gejala[31];
            tidak=gejala[31];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[32])){
            ya=gejala[37];
            tidak=gejala[32];
            if( j1==true){
                j1=false;
                selesai=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[33])){
            ya=gejala[35];
            tidak=gejala[33];
            if( j1==true){
                j1=false;
                selesai=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[34])){
            ya=gejala[34];
            tidak=gejala[34];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[35])){
            ya=gejala[35];
            tidak=gejala[35];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[36])){
            ya=gejala[38];
            tidak=gejala[36];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
                selesai=true;
            }
        }
        else if(x.equals(gejala[37])){
            ya=gejala[37];
            tidak=gejala[37];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[38])){
            ya=gejala[38];
            tidak=gejala[38];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                j2=false;
                cekkesimpulan();
            }
        }
        else if(x.equals(gejala[39])){
            ya=gejala[22];
            tidak=gejala[19];
            if( j1==true){
                selesai=false;
                j1=false;
                cekkesimpulan();
            }
            else if(j2==true){
                selesai=false;
                j2=false;
                cekkesimpulan();
            }
        }
        else{
            jTextArea1.setText("Pertanyaan tidak ada!");
        }
    }

    public void kosongkan(){
        restanya.removeAll(restanya);
        resjawaban.removeAll(resjawaban);
        jawaban.removeAll(jawaban);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        image = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        kembali = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jLabel3 = new javax.swing.JLabel();
        pilihtidak = new javax.swing.JLabel();
        pilihya = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setResizable(false);
        setSize(new java.awt.Dimension(740, 410));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
        });
        getContentPane().setLayout(null);

        image.setIcon(new javax.swing.ImageIcon(getClass().getResource("/spgangguanemosional/Gambar/image2.jpg"))); // NOI18N
        image.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        getContentPane().add(image);
        image.setBounds(220, 10, 340, 180);

        jLabel4.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 160, 158));
        jLabel4.setText("0");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(590, 200, 30, 30);

        kembali.setIcon(new javax.swing.ImageIcon(getClass().getResource("/spgangguanemosional/Gambar/kembali.jpg"))); // NOI18N
        kembali.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                kembaliMouseClicked(evt);
            }
        });
        getContentPane().add(kembali);
        kembali.setBounds(350, 350, 90, 30);

        jScrollPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 0));

        jTextArea1.setEditable(false);
        jTextArea1.setColumns(20);
        jTextArea1.setFont(new java.awt.Font("Nirmala UI", 0, 13)); // NOI18N
        jTextArea1.setLineWrap(true);
        jTextArea1.setRows(2);
        jTextArea1.setWrapStyleWord(true);
        jTextArea1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jTextArea1.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        jTextArea1.setEnabled(false);
        jScrollPane1.setViewportView(jTextArea1);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(180, 230, 430, 70);

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/spgangguanemosional/Gambar/pertanyaan.jpg"))); // NOI18N
        getContentPane().add(jLabel3);
        jLabel3.setBounds(170, 200, 450, 100);

        pilihtidak.setIcon(new javax.swing.ImageIcon(getClass().getResource("/spgangguanemosional/Gambar/tidak.jpg"))); // NOI18N
        pilihtidak.setText("Tidak");
        pilihtidak.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        pilihtidak.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pilihtidakMouseClicked(evt);
            }
        });
        getContentPane().add(pilihtidak);
        pilihtidak.setBounds(400, 310, 90, 30);

        pilihya.setIcon(new javax.swing.ImageIcon(getClass().getResource("/spgangguanemosional/Gambar/ya.jpg"))); // NOI18N
        pilihya.setText("Ya");
        pilihya.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        pilihya.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pilihyaMouseClicked(evt);
            }
        });
        getContentPane().add(pilihya);
        pilihya.setBounds(300, 310, 90, 30);

        jLabel1.setForeground(new java.awt.Color(0, 204, 204));
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/spgangguanemosional/Gambar/bg2.jpg"))); // NOI18N
        getContentPane().add(jLabel1);
        jLabel1.setBounds(0, 0, 780, 440);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void pilihyaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pilihyaMouseClicked
        jumsoal++;
        sim2=sim1;
        j1=true;
        jawaban.add(konek.kode(soal));
        pertanyaan(ya);
    }//GEN-LAST:event_pilihyaMouseClicked

    private void pilihtidakMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pilihtidakMouseClicked
        jumsoal++;
        sim2=sim1;
        j2=true;
        pertanyaan(tidak);
    }//GEN-LAST:event_pilihtidakMouseClicked

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
        Dimension posisi = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (posisi.width - this.getWidth())/2;
        int y = (posisi.height -this.getHeight())/2;
        this.setLocation(x,y);
    }//GEN-LAST:event_formWindowActivated

    private void kembaliMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_kembaliMouseClicked
        if (jumsoal>0){
            jumsoal--;
            String balik=konek.kode(restanya.get(restanya.size()-2));
            restanya.remove(restanya.size()-1);
            resjawaban.remove(resjawaban.size()-1);
            if(jawab.equals("ya")){
                if(jawaban.size()!=0){
                    jawaban.remove(jawaban.size()-1);
                }
                pertanyaan(balik);
            }else if(jawab.equals("tidak")){
                pertanyaan(balik);
            }
        }
    }//GEN-LAST:event_kembaliMouseClicked

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Interface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Interface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Interface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Interface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Interface().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel image;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    public static javax.swing.JTextArea jTextArea1;
    private javax.swing.JLabel kembali;
    private javax.swing.JLabel pilihtidak;
    private javax.swing.JLabel pilihya;
    // End of variables declaration//GEN-END:variables
}

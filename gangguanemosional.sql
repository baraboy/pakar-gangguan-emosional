-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 01 Jan 2018 pada 15.39
-- Versi Server: 5.5.27
-- Versi PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `gangguanemosional`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `diagnosis`
--

CREATE TABLE IF NOT EXISTS `diagnosis` (
  `diagnosisID` varchar(10) NOT NULL,
  `gangguanID` varchar(10) NOT NULL,
  `gejalaID` varchar(10) NOT NULL,
  PRIMARY KEY (`diagnosisID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `gangguan`
--

CREATE TABLE IF NOT EXISTS `gangguan` (
  `gangguanID` varchar(10) NOT NULL,
  `nama_gangguan` varchar(50) NOT NULL,
  `definisi` text NOT NULL,
  PRIMARY KEY (`gangguanID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gangguan`
--

INSERT INTO `gangguan` (`gangguanID`, `nama_gangguan`, `definisi`) VALUES
('A01', 'Agoraphobia', 'Jenis gangguan kecemasan di mana penderitanya akan menghindari berbagai situasi yang mungkin menyebabkan panik, atau sederhananya penderita mengalami phobia terhadap keramaian. Penderita mungkin tidak mau meninggalkan rumah. Saat berada di luar rumah, mereka bisa merasa terjebak atau malu yang akan memicu serangan panik.'),
('A02', 'Fobia Sosial', 'Rasa ketakutan ekstrem dalam situasi sosial atau yang melibatkan performa tertentu — terutama situasi yang sama sekali asing atau di mana Anda merasa akan diawasi atau dievaluasi oleh orang lain.'),
('A03', 'Fobia Khas', 'Ketakutan yang intens dan menghindari objek atau situasi tertentu. Orang dengan fobia ini mengalami kecemasan ketika   mereka   menemukan   atau   bahkan   berpikir   tentang   hal   yang mereka takuti.  Kecemasan ini terkadang mengambil bentuk serangan panik.  Namun sementara serangan pada gangguan panik terjadi tiba-tiba, serangan pada fobia khas memiliki pemicu yang sangat spesifik.'),
('A04', 'Gangguan Panik', 'Serangan panik atau panic attack adalah sebuah gelombang kecemasan dan ketakutan yang luar biasa. Jantung berdebar keras dan Anda tidak bisa bernapas. Dalam banyak kasus, serangan panik menyerang dengan tiba-tiba, tanpa peringatan apapun. Sering kali, tidak ada alasan yang jelas mengapa serangan tersebut terjadi.'),
('A05', 'Gangguan Cemas Menyeluruh', 'Gangguan cemas menyeluruh (Generalized Anxiety Disorder, GAD) merupakan kondisi gangguan yang ditandai dengan kecemasan dan kekhawatiran yang berlebihan dan tidak rasional bahkan terkadang tidak realistik terhadap berbagai peristiwa kehidupan sehari-hari.'),
('A06', 'Gangguan Campuran Anxietas dan Depresi', 'Gangguan campuran anxietas dan depresif merupakan penyakit tersendiri dan dinamakan demikian karena secara bersamaan didapati gejala-gejala depresi dan anxietas pada penderita. Adapun anxietas sendiri adalah perasaan takut yang tidak jelas dan tidak didukung oleh situasi. Ansietas atau kecemasan adalah respons emosi tanpa objek yang spesifik yang secara subjektif dialami dan dikomunikasikan secara interpersonal.'),
('A07', 'Siklotimia', 'Gangguan afeksi yang menetap yang ditandai dengan ketidakstabilan dari suasana perasaan-perasaan yang meliputi banyak periode depresi ringan yang tidak cukup parah (mood mudah berubah).'),
('A08', 'Distimia', 'Gangguan distimik (dysthymic disorder) adalah suatu kondisi kronis yang ditandai dengan gejala depresi yang terjadi hampir sepanjang hari, lebih banyak hari daripada tidak, setidaknya selama 2 tahun.'),
('A09', 'Manik Depresi', 'Gangguan mental yang menyerang kondisi psikis seseorang yang ditandai dengan perubahan suasana hati yang sangat ekstrem berupa mania dan depresi, karena itu istilah medis sebelumnya disebut dengan manic depressive. Suasana hati penderitanya dapat berganti secara tiba-tiba antara dua kutub (bipolar) yang berlawanan yaitu kebahagiaan (mania) dan kesedihan (depresi) yang berlebihan tanpa pola atau waktu yang pasti.'),
('A10', 'Depresi', 'Depresi adalah salah satu gangguan kesehatan mental yang terjadi sedikitnya selama dua minggu atau lebih yang memengaruhi pola pikir, perasaan, suasana hati (mood) dan cara menghadapi aktivitas sehari-hari.'),
('A11', 'Normal', 'Tidak ada penyakit yang terlalu serius, diasumsikan anak anda dalam pengaruh obat - obatan yang mempengaruhi emosionalnya.'),
('A12', 'Normal Tanpa gejala', 'Anak anda dalam keadaan normal. Tidak ada gejala yang mencerminkan anak anda dalam keadaan emosionalnya terganggu.'),
('A13', 'Normal', 'Anak anda dalam keadaan normal. Terdapat beberapa gejala, namun tidak ada yang perlu diperhatikan.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gejala`
--

CREATE TABLE IF NOT EXISTS `gejala` (
  `gejalaID` varchar(10) NOT NULL,
  `nama_gejala` varchar(150) NOT NULL,
  PRIMARY KEY (`gejalaID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gejala`
--

INSERT INTO `gejala` (`gejalaID`, `nama_gejala`) VALUES
('g01', 'Apakah anak anda mengalami gejala fisik seperti gatal-gatal ?'),
('g02', 'Apakah anak anda memliki masalah perasaan, seperti murung dan sedih ?'),
('g03', 'Apakah anak anda mengalami kecemasan saat berada ditempat Umum ?'),
('g04', 'Apakah anak anda merasa cemas ketika bepergian ke tempat umum ?'),
('g05', 'Apakah anak anda sering terlihat cemas ketika berpergian seorang diri ?'),
('g06', 'Apakah anak anda sering terkihat seperti berusaha menghindar dari situasi ramai, atau ditempat umum, atau ketika berpergian sendiri ?'),
('g07', 'Apakah anak anda sering terlihat merasa cemas ketika berada diluar lingkungan keluarga ?'),
('g08', 'Apakah anak anda merasa takut pada situasi tertentu ?'),
('g09', 'Apakah anak anda dalam pengaruh obat-obatan ?'),
('g10', 'Apakah anak anda sering terlihat mengalami kecemasan tanpa sebab ?'),
('g11', 'Apakah anak anda sulit berkonsentrasi ?'),
('g12', 'Apakah anak anda terlihat seperti seseorang yang selalu merasa khawatir pada nasib buruk yang menimpa ? '),
('g13', 'Apakah anak anda terlihat seperti orang yang mengalami depresi ?'),
('g14', 'Apakah anak anda mengalami gangguan ketegangan motorik ?'),
('g15', 'Apakah anak anda mengalami overaktivitas otonomik ? (gejala seperti: jantung berdebar-debar, sesa knapas, keluhan lambung, pusing, mulut kering)'),
('g16', 'Apakah anak anda mengalami kebutuhan yang berlebihan ?'),
('g17', 'Apakah anak anda mudah mengalami stress akibat masalah yang di hadapi ?'),
('g18', 'Apakah anak anda mengalami ketidakstabilan emosional dengan gejala adanya perubahan suasana perasaan (mood) ?'),
('g19', 'Apakah anak anda mengalami depresi dengan gejala sering menagis,merasa kecewa dan merasa tertekan ?'),
('g20', 'Apakah anak anda mudah mengalami perubahan emosional atau mood secara tiba-tiba ?'),
('g21', 'Apakah anak anda mengalami peningkatan aktivitas ?'),
('g22', 'Apakah gejala terkait gangguan psikologi yang terjadi pada anak anda mulai terlihat sejak dini ?'),
('g23', 'Apakah anak anda kehilangan minat terhadap sesuatu dan kurang ceria dalam kesehariannya ?'),
('g24', 'Apakah anak anda terlihat mudah lelah akibat berkurangnya energi, bukan karena beraktifitas ?'),
('g25', 'Apakah konsentrasi dan perhatian anak anda terlihat menurun ?'),
('g26', 'Apakah anak anda mengalami penurunan kepercayaan diri dan harga diri ?'),
('g27', 'Apakah anak anda selalu terlihat merasa bersalah atau tidak berguna dalam kesehariannya ?'),
('g28', 'Apakah anak anda berpandangan bahwa ia memiliki masa depan yang suram?'),
('g29', 'Apakah anak anda pernah melakukan percobaan bunuh diri ?'),
('g30', 'Apakah anak anda mengalami gangguan tidur ?'),
('g31', 'Apakah nafsu makan anak anda berkurang ?'),
('g32', 'Apakah gangguan tersebut telah dialami sekurang-kurangnya 2 minggu ?'),
('g33', 'Apakah gejala telah tampak selama sebulan ?'),
('g34', 'Apakah gejala tampak setiap hari ?'),
('g35', 'Apakah gejala telah tampak selama 1 tahun terakhir atau lebih ?'),
('g36', 'Apakah berdampak pada kelancaran aktivitas anak anda?'),
('g37', 'Apakah anak anda mengalami overaktivitas otonomik ? (gejala seperti: jantung berdebar-debar, sesa knapas, keluhan lambung, pusing, mulut kering)'),
('g38', 'Apakah gejala tampak setiap hari ?'),
('g39', 'Apakah anak anda mengalami stress seolah merasa kehidupan yang penuh masalah ?'),
('g40', 'Apakah anak anda mengalami depresi dengan gejala sering menagis,merasa kecewa dan merasa tertekan ?');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
